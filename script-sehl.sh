#!/bin/bash
mkdir -p Escritorio/pythonday

# Dependencias comunes
sudo apt-get install -y \
    ssh \
    vim \
    python2.7 \
    python-pip \
    python3-pip \
    virtualenvwrapper \
    virtualenv

#Construye tu primer API en flask
cd Escritorio
virtualenv -p /usr/bin/python3.6 apliflask
source apliflask/bin/activate
pip install \
    flask \
    ipython \
    flask-restful

#Machine Learning as a Service (MLaaS): Sirviendo soluciones inteligentes
cd Escritorio
virtualenv -p /usr/bin/python3.6 mlaas
source mlaas/bin/activate
cd pythonday
wget https://repo.anaconda.com/archive/Anaconda3-5.3.1-Linux-x86_64.sh
bash Anaconda3-5.3.1-Linux-x86_64.sh
pip install \
    tensorflow \
    keras \
    numpy \
    pandas \
    flask

#Microservicios con Python
cd Escritorio
virtualenv -p /usr/bin/python3.6 microservicios
source microservicios/bin/activate
pip install \
    requests \
    flask \
    ipython

#¿Cómo y por qué hacer logs te puede salvar de un desastre?
cd Escritorio
virtualenv -p /usr/bin/python3.6 logs
source logs/bin/activate
pip install \
    ipython \
    jupyter

#Programación para no programadores: Diseño de Interfaz Gráfica (GUI) usando Tkinter y Python
cd Escritorio
virtualenv -p /usr/bin/python3.6 programacion
source programacion/bin/activate
# FIXME: Falta agregar las dependencias de este taller
